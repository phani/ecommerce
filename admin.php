<?php
  include 'partials/_header.php';
  include 'includes/authenticate_admin.php';
?>
<div class="container">
	<div class="row alert alert-success" role="alert">
		Welcome to Admin Dasboard
	</div>
	<div class="col col-md-3"></div>
	<div class="col col-md-3">
		<a class="btn btn-primary" href="create_categories.php">Create Category</a>
	</div>
	<div class="col col-md-3">
		<a class="btn btn-primary" href="create_products.php">Create Product</a>
	</div>
</div>