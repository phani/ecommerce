<?php
  include 'partials/_header.php';
  include 'includes/authenticate_admin.php';
  $db->select('categories');
  $categories = $db->getResult();
  if(isset($_POST['productSubmit'])){
  	if($_POST['product_name'] != '' || $_POST['cat_id'] != '' || $_POST['price'] != ''){
	    $db->select('products', '*', null, 'product_name = "'.$_POST['product_name'].'"');
		if((int)$db->numRows() == 0){
			$db->insert('products', array('product_name' =>  $_POST['product_name'], 'description' => $_POST['description'], 'cat_id' => $_POST['cat_id'], 'price' => $_POST['price']));
			echo "<h2 class='text-success col col-md-offset-4'>Product created succesfully!</h2>";
		}else{
			echo "<h2 class='text-warning col col-md-offset-4'>Product already exists!</h2>";	
		}
	}else{
		echo "<h2 class='text-danger col col-md-offset-4'>Product can't be blank!</h2>";
	}
  	
  }
?>
<div class="container">
  <div class="col col-md-offset-3 col-md-4">
    <h3>Create Category</h3>
    <form role="form" name="create_products" method="post">
      <div class="form-group">
        <label for="product_name">Category name</label>
        <input type="text" class="form-control" name="product_name" placeholder="Enter product name" required>
      </div>
      <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" name="description" placeholder="Enter Description">
        </textarea>
      </div>
      <div class="form-group">
        <label for="cat_id">Category name</label>
        <select name="cat_id" class="form-control" required>
          <option>--select category--</option>
          <?php
            foreach ($categories as $value) {
          ?>
              <option value="<?= $value['cat_id'] ?>"><?= $value['category_name'] ?></option>
          <?php  } ?>          
        </select>
      </div>
      <div class="form-group">
        <label for="price">Price</label>
        <input type="number" class="form-control" name="price" placeholder="Enter price" required>
      </div>
      <button type="submit" name="productSubmit" class="btn btn-default">Submit</button>
    </form>
  </div>
</div>