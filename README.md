1. Login into phpmyadmin with as root user and with no password
2. Create database named 'commerce'
3. Import commerce.sql into phpmyadmin in commerce database
4. This creates the tables.
5. Normal user can give your details to register/login
5. Admin can login with credentials
     admin
     test1234
6. You can change these values based upon your requirements in (includes/mysql_crud.php)
   Defaults:
   	private $db_host = "localhost";  
	private $db_user = "root";  
	private $db_pass = "";  
	private $db_name = "commerce";