<?php 
  include 'partials/_header.php';
  include 'includes/user.php';
  $customer_id = $current_user['customer_id'];
  $db->select('orders', '*', null, 'customer_id = "'.$customer_id.'"');
  $results = [];
  if((int)$db->numRows() > 0){
    $db->select('customers', '*', null, 'customer_id = "'.$customer_id.'"');
    $db->sql('select  * FROM `orders`, `customers`, `products` where `orders`.customer_id ='.$current_user['customer_id'].' AND `customers`.customer_id = `orders`.customer_id AND `products`.product_id = `orders`.product_id;');
    $results = $db->getResult();
  }
?>
<h2>My Orders</h2>
<?php if((int)$db->numRows() == 0){ ?>
 <span class="col col-md-offset-4"> No Orders yet! <a class="btn btn-sm btn-success" href="categories.php">order one</a></span>
<?php } ?>
<table class="table table-bordered">
  <thead>
  	<tr class="bg-warning">
  		<?php foreach ($results as $value) {
  			foreach ($value as  $column => $column_value) {
  			  echo '<td>'.$column.'</td>';	
  			}
  		}
  		?>  		
  	</tr>
  </thead>
  <tbody>
  	<?php 
  		foreach ($results as $value) {
  			echo '<tr>';
  			foreach ($value as  $column => $column_value) {
  			  echo '<td>'.$column_value.'</td>';	
  			}
  			echo '</tr>';
  		}
  	?>  	
  </tbody>
</table>