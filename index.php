<?php 
  include __DIR__ . '/partials/_header.php';
  if(isset($_SESSION['current_user']))
    header('Location: categories.php');
?>
<div class="container">
	<div class="jumbotron">
	  <h1>E-Commerce</h1>
	  <p>Watch, choose, buy!</p>
	  <p><a class="btn btn-primary btn-lg" href="loginprocessor.php" role="button">Login to start</a></p>
	</div>
</div>