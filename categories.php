<?php
  include 'partials/_header.php';
  include 'includes/authenticate_user.php';
  $cat = isset($_GET['id']) ? 'cat_id in ('.$_GET['id'].')' : null;  
  $db->select('categories', '*', null, $cat);
  $res = $db->getResult(); 
?>
<div class="container">
  <div class="col col-md-offset-4">
    <h2 class="text-primary">Categories</h2>
  </div>
</div>
<?php
  foreach ($res as $value) {
  ?>  
  <div class="col col-md-4">
    <div class="panel panel-default">
      <a href="<?php echo 'products.php?cat_id='.$value['cat_id']?>">
        <div class="panel-body">
          <?php echo $value['category_name'] ?>
        </div>
      </a>    
    </div>
  </div>
  <?php
  }
