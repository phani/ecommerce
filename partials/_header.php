<?php 
  session_start();
  include('includes/mysql_crud.php');
  $db = new Database();
  $db->connect();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>E-Commerce</title>

    
    <link href="assets/stylesheets/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body> 
    <div class="navbar navbar-inverse">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">E-Commerce</a>
      </div>
      <div class="navbar-collapse collapse navbar-inverse-collapse">
        <ul class="nav navbar-nav">
          <?php if(isset($_SESSION['current_user'])){ ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Categories <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <?php
                  $db->select('categories', '*', null, null, null, 4);
                  $res = $db->getResult(); 
                  foreach ($res as $value) {
                    echo '<li><a href="products.php?cat_id='.$value['cat_id'].'">'.$value['category_name'].'</a></li>';
                  }
                  if($db->numRows() > 1)
                    echo '<li class="divider"></li><li><a href="categories.php">All Categories</a></li>';
                ?>
              </ul>
            </li>
            <?php } ?>
        </ul>
        
        <ul class="nav navbar-nav navbar-right">                    
          <?php if(isset($_SESSION['is_admin']) && $_SESSION['is_admin'] == true ){ ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="admin.php">Dashboard</a></li>
                <li><a href="transactions.php">Transactions</a></li>              
              </ul>
            </li>
          <?php }?>
          <?php if(isset($_SESSION['current_user']) && (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] == false) ){ ?>
            <li><a href="registrationprocessor.php">Customer</a></li>
            <li><a href="my_orders.php">My Orders</a></li>
          <?php } ?>
          <li><a href="<?= isset($_SESSION['current_user']) ? 'logout.php' : 'loginprocessor.php'; ?>"><?= isset($_SESSION['current_user']) ? 'Logout' : 'Login'; ?></a></li>
        </ul>
      </div>
    </div>
    <script src="assets/javascripts/jquery.min.js"></script>
    
    <script src="assets/javascripts/bootstrap.min.js"></script>
  