<?php 
  include 'partials/_header.php';
  include 'includes/user.php';
  include 'includes/authenticate_user.php';
  if($current_user['customer_id'] != null){
    $db->select('customers', '*', null, 'customer_id = "'.$current_user['customer_id'].'"');
    $numRows = (int)$db->numRows();

  }else{
    $numRows = 0;
  }
  $customer = array( 'firstname' => '', 'lastname' => '', 'street' => '', 'city' => '', 'state' => '', 'zip' => '', 'country' => '', 'phone' => '', 'email' => '' ); 
  
  if($numRows > 0){
    $customer = $db->getResult()[0];
    
    if(isset($_POST['registrationSubmit']))
      foreach ($customer as $key => $value) {
        if($key != 'customer_id')
          $customer[$key] = $_POST[$key];
      }
    if(isset($_GET['login'])){
      header('Location: categories.php');
    }
    if(isset($_POST['registrationSubmit'])){
      $db->update('customers', $customer, 'customer_id="'.$current_user['customer_id'].'"');
      echo '<h2 class="text-success col col-md-offset-1">Details updated successfully!</h2>';
    }
  }else{
    if(isset($_POST['registrationSubmit'])){  
      foreach ($customer as $key => $value) {
        $customer[$key] = $_POST[$key];
      }    
      if($db->insert('customers', $customer)){
        $current_user['customer_id'] = $db->getResult()[0];
        $db->update('logins', $current_user, 'login_id="'.$_SESSION['current_user'].'"');
        header('Location: categories.php');
      }
    }
  }
?>
<div class="container">
  <div class="col col-md-6">
    <form class="form-horizontal" method="post" name="registrationForm">
      <fieldset>
        <legend>Customer</legend>
        <div class="form-group">
          <label for="firstname" class="col-lg-2 control-label">Firstname</label>
          <div class="col-lg-10">
            <input type="text" value="<?php echo $customer['firstname'];?>" class="form-control" name="firstname" placeholder="Firstname" required>
          </div>
        </div>
        <div class="form-group">
          <label for="lastname" class="col-lg-2 control-label">Lastname</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" value="<?php echo $customer['lastname'];?>" name="lastname" placeholder="Lastname" required>        
          </div>
        </div>
        <div class="form-group">
          <label for="email" class="col-lg-2 control-label">Email</label>
          <div class="col-lg-10">
            <input type="email" class="form-control" value="<?php echo $customer['email']; ?>" name="email" placeholder="Email" required>        
          </div>
        </div>
        <div class="form-group">
          <label for="street" class="col-lg-2 control-label">Street</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" value="<?php echo $customer['street'];?>" name="street" placeholder="Street">        
          </div>
        </div>
        <div class="form-group">
          <label for="city" class="col-lg-2 control-label">City</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" value="<?php echo $customer['city'];?>" name="city" placeholder="City">        
          </div>
        </div>
        <div class="form-group">
          <label for="state" class="col-lg-2 control-label">State</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" name="state" value="<?php echo $customer['state'];?>" placeholder="State">        
          </div>
        </div>
        <div class="form-group">
          <label for="Zip" class="col-lg-2 control-label">Zip</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" name="zip" value="<?php echo $customer['zip'];?>" placeholder="Zip">        
          </div>
        </div>
        <div class="form-group">
          <label for="country" class="col-lg-2 control-label">Country</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" name="country" value="<?php echo $customer['country'];?>" placeholder="Country">        
          </div>
        </div>
        <div class="form-group">
          <label for="phone" class="col-lg-2 control-label">Phone</label>
          <div class="col-lg-10">
            <input type="phone" class="form-control" name="phone" value="<?php echo $customer['phone'];?>" placeholder="Phone">        
          </div>
        </div>
        <div class="form-group">
          <div class="col col-md-offset-5">
            <button type="submit" name="registrationSubmit" class="btn btn-success btn-small">Submit</button>
          </div>
        </div>
      </fieldset>
    </form>
  </div>
</div>