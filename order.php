<?php
  include 'partials/_header.php';
  include 'includes/authenticate_user.php';
  if(isset($_GET['product_name']))
  	$productname = $_GET['product_name'];
  else
  	header('Location: products.php');
?>
<div class="container">
  <div class="col col-md-offset-3 col-md-4">
    <h3>Enter Details to Purcahse</h3>
    <form role="form" name="purchase" method="post" action="purchase.php">
      <div class="form-group">
        <label for="firstname">Firstname</label>
        <input type="text" class="form-control" name="firstname" placeholder="firstname" required>
      </div>
      <div class="form-group">
        <label for="lastname">Lastname</label>
        <input type="text" class="form-control" name="lastname" placeholder="Lastname" required>
      </div>
      <div class="form-group">
        <label for="product_name">Productname</label>
        <input type="text" class="form-control" value="<?= $productname?>" name="product_name" placeholder="Productname" readonly>
      </div>
      <div class="form-group">
        <label for="quantity">Quantity</label>
        <input type="number" class="form-control" name="quantity" placeholder="Quantity" required>
      </div>
      <button type="submit" name="orderSubmit" class="col-md-offset-4 btn btn-success">Buy</button>
    </form>
  </div>
</div>