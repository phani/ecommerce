<?php
  include 'partials/_header.php';
  include 'includes/authenticate_admin.php';
  $db->sql('select  * FROM `orders`, `customers`, `products` where `customers`.customer_id = `orders`.customer_id AND `products`.product_id = `orders`.product_id;');
  $results = $db->getResult();
?>
<h2>Transactions</h2>
<table class="table table-bordered">
  <thead>
  	<tr class="bg-warning">
  		<?php 
      $count = 0;
      foreach ($results as $value) {
        $count = 1;
  			foreach ($value as  $column => $column_value) {
  			  echo '<td>'.$column.'</td>';	
  			}
        if($count == 1)
          break;
  		}
  		?>  		
  	</tr>
  </thead>
  <tbody>
  	<?php 
  		foreach ($results as $value) {
  			echo '<tr>';
  			foreach ($value as  $column => $column_value) {
  			  echo '<td>'.$column_value.'</td>';	
  			}
  			echo '</tr>';
  		}
  	?>  	
  </tbody>
</table>