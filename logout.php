<?php
  include 'partials/_header.php';
  unset($_SESSION['current_user']);
  unset($_SESSION['is_admin']);
  header('Location: index.php');
?>
