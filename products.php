<?php
  include 'partials/_header.php';
  include 'includes/authenticate_user.php';
  $cat = isset($_GET['cat_id']) ? 'cat_id in ('.$_GET['cat_id'].')' : null;  
  $db->select('products', '*', null, $cat, 'cat_id');
  $res = $db->getResult(); 
?>
  <div class="container">
    <a class="btn btn-info" href="products.php">All Products</a>
    <a class="btn btn-warning" href="categories.php">All Categories</a>
  </div><br/>
  <?php
  foreach ($res as $value) {
  ?>
  <div class="col col-md-4">
  	<div class="panel panel-default">
  	  <div class="panel-heading">
  	    <h3 class="panel-title"><?php echo $value['product_name']; ?></h3>
  	  </div>
  	  <div class="panel-body">
  	    Description: <?php echo $value['description']; ?><br />
  	    Price: <?php echo $value['price']; ?>
  	  </div>
  	  <div class="panel-footer">
  	  	Category: <?php 
  	  		$db->select('categories', '*', null, 'cat_id ='.$value['cat_id'], null, 1);
      		echo $db->getResult()[0]['category_name'];
        ?>
        <a class="btn btn-success pull-right btn-xs" href="order.php?product_name=<?= $value['product_name']?>">Purchase</a>
  	  </div>
  	</div> 
  </div>    
  <?php
  } 
