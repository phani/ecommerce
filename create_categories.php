<?php
  include 'partials/_header.php';
  include 'includes/authenticate_admin.php';
  if(isset($_POST['categorySubmit'])){
  	if($_POST['category_name'] != ''){
	    $db->select('categories', '*', null, 'category_name = "'.$_POST['category_name'].'"');
		if((int)$db->numRows() == 0){
			$db->insert('categories', array('category_name' =>  $_POST['category_name']));
			echo "<h2 class='text-success col col-md-offset-4'>Category created succesfully!</h2>";
		}else{
			echo "<h2 class='text-warning col col-md-offset-4'>Category already exists!</h2>";	
		}
	}else{
		echo "<h2 class='text-danger col col-md-offset-4'>Category can't be blank!</h2>";
	}
  	
  }
?>
<div class="container">
  <div class="col col-md-offset-3 col-md-4">
    <h3>Create Category</h3>
    <form role="form" name="create_categorys" method="post">
      <div class="form-group">
        <label for="category_name">Category name</label>
        <input type="text" class="form-control" name="category_name" placeholder="Enter categoryname" required>
      </div>
      <button type="submit" name="categorySubmit" class="btn btn-default">Submit</button>
    </form>
  </div>
</div>