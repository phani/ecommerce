<?php 
  include 'partials/_header.php'; 
  if(isset($_SESSION['current_user']))
    header('Location: categories.php');
?>
<div class="container">
  <div class="col col-md-offset-3 col-md-4">
    <h3>Login or Register</h3>
    <form role="form" name="login" method="post">
      <div class="form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" name="username" placeholder="Enter username" required>
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password" placeholder="Password" required>
      </div>
       <div class="form-group">
        <label for="user_role">Login as</label>
        <select name="user_role" class="form-control" required>
          <option value="user">User</option>
          <option value="admin">Admin</option>          
        </select>
      </div>
      <button type="submit" name="submit" class="btn btn-default">Submit</button>
    </form>
  </div>
</div>
<?php
  if(isset($_POST['submit'])){
    $username = $_POST['username'];
    $password = $_POST['password'];
    $user_role = $_POST['user_role'];
    if($username != '' && $password != ''){
      if($user_role == 'admin'){
        $id = 'admin_id';
        $name = 'admin_name = "'.$username.'"';
        $pass =  'admin_password ="'.md5($password).'"';
        $table = 'admins';
      }else {
        $id = 'login_id';
        $name = 'username = "'.$username.'"';
        $pass =  'password ="'.md5($password).'"';
        $table = 'logins';
      }
      $db->select($table, '*', null, $name);
      $_SESSION['is_admin'] = false;
      if((int)$db->numRows() > 0){
        $db->select($table, '*', null, $name.' AND '.$pass);
        if((int)$db->numRows() > 0){
          $_SESSION['current_user'] = $db->getResult()[0][$id];          
          if($user_role == 'admin'){
            $_SESSION['is_admin'] = true;
            header('Location: admin.php');
          }else{           
            header('Location: registrationprocessor.php?login='.true); 
          } 
        }else{
           header('Location: index.php');
        }
      }else{
        $values = array('username' => $username, 'password'=> md5($password));
        if($db->insert('logins', $values)){
          $_SESSION['current_user'] = $db->getResult()[0];          
          header('Location: registrationprocessor.php');
        }
      }
    }else{
      echo '<script type="text/javascript"> alert("Username and Password Required !") </script>';
    }
  }